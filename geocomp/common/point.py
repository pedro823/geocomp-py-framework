#!/usr/bin/env python

from vector import Vector

class Point:
    "Um ponto representado por suas coordenadas cartesianas"

    def __init__ (self, *args):
        "Para criar um ponto, passe suas coordenadas."
        if len(args) == 0:
            raise ValueError("Point must have at least one coordinate")
        self.__coords = list(args)
        self.polygon_id = -1
        self.lineto_id = {}

    def __repr__ (self):
        "Retorna uma string da forma '( x1 x2 x3 ... xn )'"
        res = "("
        for i in self.__coords:
            res += " " + repr(i) + ","
        return res[:-1] + " )"

    def __add__(self, other):
        "Adiciona um ponto a um vetor ou a outro ponto"
        if isinstance(other, Vector):
            if other.dimension != len(self.__coords):
                raise ValueError(('Cannot add point with dimension {}'
                                 + ' with vector with dimension {}').format(
                                    len(self.__coords), other.dimension
                                 ))
            return Point(*[a + b for a, b in zip(self.__coords, other.values)])
        if isinstance(other, Point):
            return Point(self.x + other.x, self.y + other.y)
        raise ValueError('Cannot add point and {}'.format(type(other)))

    def __sub__(self, other):
        "Subtrai um ponto a um vetor ou a outro ponto"
        if isinstance(other, Vector):
            return self.__add__(other * -1)
        if isinstance(other, Point):
            return Point(self.x - other.x, self.y - other.y)
        raise ValueError('Cannot subtract point and {}'.format(type(other)))


    def __eq__(self, other):
        if self is other:
            return True
        if type(self) != type(other):
            return False
        return self.x == other.x and self.y == other.y

    def __hash__(self):
        return 31 * hash(self.x) + hash(self.y)

    def __check_boundary(self, index):
        "Checa se o índice requerido existe no ponto"
        if index < 0 or index >= len(self.__coords):
            raise ValueError("Point does not have dimension {}".format(index))

    @property
    def x(self):
        self.__check_boundary(0)
        return self.__coords[0]

    @x.setter
    def x(self, x):
        self.__check_boundary(0)
        self.__coords[0] = x

    @property
    def y(self):
        self.__check_boundary(1)
        return self.__coords[1]

    @y.setter
    def y(self, y):
        self.__check_boundary(1)
        self.__coords[1] = y

    @property
    def z(self):
        self.__check_boundary(2)
        return self.__coords[2]

    @z.setter
    def z(self, z):
        self.__check_boundary(2)
        self.__coords[2] = z

    def __getitem__(self, key):
        self.__check_boundary(key)
        return self.__coords[key]

    def __setitem__(self, key, value):
        self.__check_boundary(key)
        self.__coords[key] = value

    def __lt__(self, other):
        if self is other:
            return True
        if type(self) != type(other):
            return False
        if self.y < other.y:
            return True
        if self.y > other.y:
            return False
        if self.x < other.x:
            return True
        return False

    def __le__(self, other):
        if self is other:
            return True
        if type(self) != type(other):
            return False
        if self.y < other.y:
            return True
        if self.y > other.y:
            return False
        if self.x < other.x:
            return True
        if self.x > other.x:
            return False
        return True
